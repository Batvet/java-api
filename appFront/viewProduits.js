document.addEventListener("DOMContentLoaded", function() {

    // Effectuer une requête GET vers l'API avec le jeton d'authentification
    fetch('http://localhost:8080/api-0.0.1/api/products', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiYXVkIjoiYXBpIiwiaXNzIjoiL3NlY3VyaXR5IiwiZXhwIjoxNzExNDM4Njk3LCJpYXQiOjE3MTA4MzM4OTcsImp0aSI6IjRiODZhZDViLWZiYmYtNDU5Ni1iN2I1LTI5ZGQ2Y2JiOGJmYiJ9.JnfQdZPcHO_JvmKKkUdhduSQEU2TjCK3dkYA8sKpMulwARPJlFSTjuKU0ipVZt1RrA6EcCwTb623MNx1f4b0r4W9sG-F_6kBloQnwF_K_sYpkOa0syMbCv9wksn0Sf3qCDP1QMNcldaO050FPveD6gbyU1Lm8tqrsRsu8B0jiVgsQp0pKIliOSL-JUHXPyeS9nzuz8HSzwhx-eKJKftu8IDCKO1Nklkc49IWEat6At4vEPjC9Q7csdnHYHcpBhYOF_Zmr2Gj_mw9S9XZ4GD7uPVZMYAlmjyUhkJnmCYn6QACl7irs8R3E8tTChV9bsbgpAMH_U0KUAq7MbNoWLoQdQE08WreAB4vJTlIEmvbB0idPnrGRfWdD0Cl8Rjnlt0eq2smBft-Ovc-lBe94H35jZ9j7-MAb4fvHtPREBUpGfFtttilh8mMl4favL1kTQAWuxEPxPgRstPbWvrAuSneVF14JOTUiQf7gtN63ATDd-4iUU2AeIVH5LE1hjH4r5c6Z-Z6xr41CanDTLOX_7fz98mYiRtSbcit267gGSHKbEw2DZz8-UIh4NY4DSru-XRPlk2_zD2uWU1diRcLowC9BA'
        }
    })
    .then(response => response.json())
    .then(data => {
            // Récupérer l'élément tbody pour y ajouter les produits
            const productsList = document.getElementById("products-list");

            // Parcourir les données des produits et les ajouter au tableau
            data.forEach(product => {
                const tr = document.createElement("tr");
                tr.innerHTML = `
                    <td>${product.id}</td>
                    <td>${product.label}</td>
                    <td>${product.currentBid}</td>
                    <td>${product.sellerId}</td>
                `;
                productsList.appendChild(tr);
            });
        })
        .catch(error => {
            console.error('Une erreur s\'est produite lors de la récupération des produits :', error);
        });

            // Récupérer le bouton de retour
    const backBtn = document.getElementById("back-btn");

    // Gestionnaire d'événements pour le clic sur le bouton de retour
    backBtn.addEventListener("click", function() {
        // Rediriger vers la page précédente
        window.history.back();
    });
});
