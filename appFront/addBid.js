document.addEventListener("DOMContentLoaded", function() {
    const addAuctionForm = document.getElementById("add-auction-form");
    const productIdSelect = document.getElementById("product-id");

    // Gestionnaire d'événements pour la soumission du formulaire
    addAuctionForm.addEventListener("submit", function(event) {
        event.preventDefault();
/*
        // Récupérer la liste des produits depuis l'API
    fetch('http://localhost:8080/api-0.0.1/api/Bids', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiYXVkIjoiYXBpIiwiaXNzIjoiL3NlY3VyaXR5IiwiZXhwIjoxNzExNDM4Njk3LCJpYXQiOjE3MTA4MzM4OTcsImp0aSI6IjRiODZhZDViLWZiYmYtNDU5Ni1iN2I1LTI5ZGQ2Y2JiOGJmYiJ9.JnfQdZPcHO_JvmKKkUdhduSQEU2TjCK3dkYA8sKpMulwARPJlFSTjuKU0ipVZt1RrA6EcCwTb623MNx1f4b0r4W9sG-F_6kBloQnwF_K_sYpkOa0syMbCv9wksn0Sf3qCDP1QMNcldaO050FPveD6gbyU1Lm8tqrsRsu8B0jiVgsQp0pKIliOSL-JUHXPyeS9nzuz8HSzwhx-eKJKftu8IDCKO1Nklkc49IWEat6At4vEPjC9Q7csdnHYHcpBhYOF_Zmr2Gj_mw9S9XZ4GD7uPVZMYAlmjyUhkJnmCYn6QACl7irs8R3E8tTChV9bsbgpAMH_U0KUAq7MbNoWLoQdQE08WreAB4vJTlIEmvbB0idPnrGRfWdD0Cl8Rjnlt0eq2smBft-Ovc-lBe94H35jZ9-MAb4fvHtPREBUpGfFtttilh8mMl4favL1kTQAWuxEPxPgRstPbWvrAuSneVF14JOTUiQf7gtN63ATDd-4iUU2AeIVH5LE1hjH4r5c6Z-Z6xr41CanDTLOX_7fz98mYiRtSbcit267gGSHKbEw2DZz8-UIh4NY4DSru-XRPlk2_zD2uWU1diRcLowC9BA'
        },
        body: JSON.stringify(newAuction)
    })
    .then(response => response.json())
    .then(data => {
        // Ajouter chaque produit comme option dans le menu déroulant
        data.forEach(product => {
            const option = document.createElement("option");
            option.value = product.id;
            option.textContent = product.id; // Afficher l'ID du produit
            productIdSelect.appendChild(option);
        });
    })
    .catch(error => {
        console.error('Une erreur s\'est produite lors de la récupération des produits :', error);
    });

// Mettre à jour le label de l'ID du produit lorsque l'utilisateur sélectionne un produit
productIdSelect.addEventListener("change", function() {
    const selectedProductId = productIdSelect.value;
    productIdLabel.textContent = "ID du produit: " + selectedProductId;
});
console.log;
*/
        // Récupérer les valeurs du formulaire
        const bidAmount = document.getElementById("bid-amount").value;
        //const productId = productIdSelect.value; // Utiliser la valeur sélectionnée dans la liste déroulante
        const productId = document.getElementById("product-id").value;
        const sellerId = document.getElementById("seller-id").value;
        const message = document.getElementById("message").value;

        // Créer un nouvel objet d'enchère avec les données du formulaire
        const newAuction = {
            bid: parseInt(bidAmount),
            idProduct: parseInt(productId),
            idSeller: parseInt(sellerId),
            message: message,
            dateCreate: Date.now(),
            dateUpdate: Date.now(),
            active: true
        };

        // Effectuer une requête POST vers l'API pour ajouter l'enchère
        fetch('http://localhost:8080/api-0.0.1/api/Bids', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiYXVkIjoiYXBpIiwiaXNzIjoiL3NlY3VyaXR5IiwiZXhwIjoxNzExNDM4Njk3LCJpYXQiOjE3MTA4MzM4OTcsImp0aSI6IjRiODZhZDViLWZiYmYtNDU5Ni1iN2I1LTI5ZGQ2Y2JiOGJmYiJ9.JnfQdZPcHO_JvmKKkUdhduSQEU2TjCK3dkYA8sKpMulwARPJlFSTjuKU0ipVZt1RrA6EcCwTb623MNx1f4b0r4W9sG-F_6kBloQnwF_K_sYpkOa0syMbCv9wksn0Sf3qCDP1QMNcldaO050FPveD6gbyU1Lm8tqrsRsu8B0jiVgsQp0pKIliOSL-JUHXPyeS9nzuz8HSzwhx-eKJKftu8IDCKO1Nklkc49IWEat6At4vEPjC9Q7csdnHYHcpBhYOF_Zmr2Gj_mw9S9XZ4GD7uPVZMYAlmjyUhkJnmCYn6QACl7irs8R3E8tTChV9bsbgpAMH_U0KUAq7MbNoWLoQdQE08WreAB4vJTlIEmvbB0idPnrGRfWdD0Cl8Rjnlt0eq2smBft-Ovc-lBe94H35jZ9-MAb4fvHtPREBUpGfFtttilh8mMl4favL1kTQAWuxEPxPgRstPbWvrAuSneVF14JOTUiQf7gtN63ATDd-4iUU2AeIVH5LE1hjH4r5c6Z-Z6xr41CanDTLOX_7fz98mYiRtSbcit267gGSHKbEw2DZz8-UIh4NY4DSru-XRPlk2_zD2uWU1diRcLowC9BA'
            },
            body: JSON.stringify(newAuction)
        })
        .then(response => {
            if (response.ok) {
                alert("Enchère ajoutée avec succès !");
                window.location.href = "index.html"; // Rediriger vers index.html
            } else {
                throw new Error('Erreur lors de l\'ajout de l\'enchère.');
            }
        })
        .catch(error => {
            console.error('Une erreur s\'est produite lors de l\'ajout de l\'enchère :', error);
        });
    });
});
