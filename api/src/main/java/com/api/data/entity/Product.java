package com.api.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

import java.util.Date;

@Data
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "label cannot be empty.")
    @Column(name = "label")
    private String label;

    @NotNull(message = "currentBid cannot be null.")
    @Column(name = "current_bid")
    private double currentBid;

    @NotNull(message = "sellerId cannot be null.")
    @Column(name = "id_seller")
    private int sellerId;
}
