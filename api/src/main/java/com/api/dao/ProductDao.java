package com.api.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

import com.api.data.entity.Product;
import com.api.exception.business.ResourceStateConflictException;
import com.api.exception.technical.DAOException;
import com.api.jpa.bootstrap.config.JpaEntityManager;

@Stateless
public class ProductDao {

    public List<Product> getAll() throws DAOException {
        EntityManager entityManager = null;
        try {
            entityManager = JpaEntityManager.getEntityManagerFactory().createEntityManager();
            return entityManager.createQuery("SELECT p FROM Product p", Product.class).getResultList();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public Product get(long id) throws DAOException {
        EntityManager entityManager = null;
        try {
            entityManager = JpaEntityManager.getEntityManagerFactory().createEntityManager();
            return entityManager.find(Product.class, id);
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public void create(Product entity) throws DAOException {
        EntityManager entityManager = null;
        try {
            entityManager = JpaEntityManager.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(entity);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public void update(Product entity) throws DAOException {
        EntityManager entityManager = null;
        try {
            entityManager = JpaEntityManager.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.merge(entity);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public void delete(Product entity) throws DAOException, ResourceStateConflictException {
        EntityManager entityManager = null;
        try {
            entityManager = JpaEntityManager.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();
            entity = entityManager.merge(entity);
            entityManager.remove(entity);
            entityManager.getTransaction().commit();
        } catch (RollbackException e) {
            throw new ResourceStateConflictException(e);
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }
}
