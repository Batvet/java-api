package com.api.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.api.business.ProductBusiness;
import com.api.data.entity.Product;
import com.api.exception.business.InvalidElementException;
import com.api.exception.business.NotFoundException;
import com.api.exception.business.ResourceStateConflictException;
import com.api.exception.technical.DAOException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@Path("products")
@Tag(name = "Products")
public class ProductResource {
    @Inject
    private ProductBusiness productBusiness;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get all Products", description = "Get all Products", responses = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Product.class)))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error") })
    @SecurityRequirement(name = "bearer-auth")
    public Response getProducts() throws DAOException {
        return Response.ok(productBusiness.getAll()).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a Product", description = "Get a Product by id", responses = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))),
            @ApiResponse(responseCode = "404", description = "Product not found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error") })
    @SecurityRequirement(name = "bearer-auth")
    public Response getProduct(@PathParam("id") long id) throws NotFoundException, DAOException {
        return Response.ok(productBusiness.get(id)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add Product", description = "Create a Product in the database", responses = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))),
            @ApiResponse(responseCode = "400", description = "Invalid Product"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error") })
    @SecurityRequirement(name = "bearer-auth")
    public Response addProduct(Product product) throws InvalidElementException, DAOException {
        return Response.ok(productBusiness.add(product)).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update Product", description = "Update a Product in the database", responses = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))),
            @ApiResponse(responseCode = "400", description = "Invalid Product"),
            @ApiResponse(responseCode = "404", description = "Product not found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error") })
    @SecurityRequirement(name = "bearer-auth")
    public Response updateProduct(@PathParam("id") long id, Product product) throws NotFoundException, InvalidElementException, DAOException {
        return Response.ok(productBusiness.update(id, product)).build();
    }

    @DELETE
	@Path("{id}")
	@Operation(summary = "Delete Product", description = "Delete a Product by id.", responses = {
			@ApiResponse(responseCode = "200", description = "Success"),
			@ApiResponse(responseCode = "409", description = "Product not found"),
			@ApiResponse(responseCode = "500", description = "Internal Server Error") })
	@SecurityRequirement(name = "bearer-auth")
	public Response deleteProduct(@PathParam("id") long id) throws ResourceStateConflictException, DAOException {
		productBusiness.delete(id);
		return Response.noContent().build();
	}
}
