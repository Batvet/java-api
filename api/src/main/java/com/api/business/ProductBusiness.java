package com.api.business;

import java.util.List;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.api.dao.ProductDao;
import com.api.data.entity.Product;
import com.api.exception.business.NotFoundException;
import com.api.exception.technical.DAOException;
import com.api.exception.business.ResourceStateConflictException;

@Stateless
public class ProductBusiness {
    @Inject
    private ProductDao productDao;

    public List<Product> getAll() throws DAOException {
        return productDao.getAll();
    }

    public Product get(long id) throws DAOException, NotFoundException {
        Product product = productDao.get(id);
        if (Objects.isNull(product)) {
            throw new NotFoundException("Product with id " + id + " not found");
        }
        return product;
    }

    public Product add(Product product) throws DAOException {
        productDao.create(product);
        return productDao.get(product.getId());
    }

    public Product update(long id, Product updatedProduct) throws DAOException, NotFoundException {
        Product existingProduct = get(id);
        existingProduct.setLabel(updatedProduct.getLabel());
        existingProduct.setCurrentBid(updatedProduct.getCurrentBid());
        // Update other fields as needed
        productDao.update(existingProduct);
        return productDao.get(existingProduct.getId());
    }

    public void delete(long id) throws DAOException, ResourceStateConflictException {
		Product product = productDao.get(id);
        productDao.delete(product);
	}
}
