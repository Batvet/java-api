# Bids API & front-end

## Pour lancer l'API :

docker build -t dev-toolbox:latest -f dev-toolbox.dockerfile .

docker-compose up

docker-compose -f deploy.yaml up

## Pour tester les fonctions de l'API avec swagger :

http://localhost:8080/api-0.0.1/swagger-ui/#/

## Pour tester l'implémentation avec notre interface :

Dans le dossier appFront, lancer **index.html** dans un navigateur
